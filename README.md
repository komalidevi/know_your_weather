This is a simple weather application developed using Python's Tkinter library and OpenWeatherMap API to fetch current weather data of a city.

## How to Use

1. Clone the repository.
2. Install the required dependencies using `pip install -r requirements.txt`.
3. Run the `forecast.py` file.
4. Enter the name of the city for which you want to check the weather.
5. Click on the "Submit" button to fetch the weather information.

## Features

- Displays current weather conditions including temperature, weather condition, atmospheric pressure, wind speed, cloudiness, sunrise time, and current time.
- Saves weather data to a SQLite database for future reference.
- Background image adds aesthetic appeal to the application.

## Requirements

- Python 3.x
- tkinter
- requests
- PIL (Python Imaging Library)

## How It Works

- The application uses the OpenWeatherMap API to fetch weather data based on the user's input city.
- Upon clicking the "Submit" button, the application makes an API call and retrieves the weather data.
- The retrieved data is then displayed on the GUI.
- Additionally, the weather data is saved to a SQLite database for historical records.

## Note

- Before running the application, make sure to replace `'YOUR_API_KEY'` with your actual OpenWeatherMap API key in the code.
- The background image file should be named "Background.jpg" and should be placed in the same directory as the code file.

