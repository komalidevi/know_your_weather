import sqlite3

# Function to check if data is stored in the database
def check_database():
    conn = sqlite3.connect('weather.db')
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM WeatherData''')
    data = cursor.fetchall()
    conn.close()
    
    if data:
        print("Data stored in the database:")
        for row in data:
            print(row)
    else:
        print("No data stored in the database.")

# Call the function to check the database
check_database()
