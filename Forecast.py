import tkinter as tk
from tkinter import ttk, messagebox
import requests
from datetime import datetime
import sqlite3
from PIL import Image, ImageTk

# Function to fetch weather data
def fetch_weather():
    city = city_entry.get()
    api_key = 'e3b0c934a776170ad62022f4406f7721'  # Replace 'YOUR_API_KEY' with your actual OpenWeatherMap API key
    url = f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={api_key}'
    response = requests.get(url)
    data = response.json()
    if data['cod'] == 200:
        temp_celsius = round(data['main']['temp'] - 273.15)
        weather_info = f"{data['name']}, {data['sys']['country']}: {temp_celsius}°C\n"
        weather_info += f"Weather Condition: {data['weather'][0]['description']}\n"
        weather_info += f"Atmospheric Pressure: {data['main']['pressure']}hPa\n"
        weather_info += f"Wind Speed: {data['wind']['speed']} meter/sec\n"
        weather_info += f"Cloudiness: {data['clouds']['all']}%\n"
        sunrise_time = datetime.fromtimestamp(data['sys']['sunrise']).strftime('%I:%M %p')
        weather_info += f"Sunrise: {sunrise_time}\n"
        current_time = datetime.now().strftime('%A, %B %d, %Y %I:%M %p')
        weather_info += f"Current Time: {current_time}"
        output_label.config(text=weather_info)
        # Save data to SQLite database
        save_to_database(city, temp_celsius, data['weather'][0]['description'], data['main']['pressure'], data['wind']['speed'], data['clouds']['all'], sunrise_time, current_time)
    else:
        messagebox.showerror("Error", "Couldn't be processed. Your city name is not valid.")

# Function to save weather data to SQLite database
def save_to_database(city, temperature, weather_condition, pressure, wind_speed, cloudiness, sunrise_time, current_time):
    conn = sqlite3.connect('weather.db')
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS WeatherData
                      (City TEXT, Temperature INTEGER, WeatherCondition TEXT,
                      Pressure INTEGER, WindSpeed REAL, Cloudiness INTEGER,
                      Sunrise TEXT, CurrentTime TEXT)''')
    cursor.execute('''INSERT INTO WeatherData VALUES (?, ?, ?, ?, ?, ?, ?, ?)''',
                   (city, temperature, weather_condition, pressure, wind_speed, cloudiness, sunrise_time, current_time))
    conn.commit()
    conn.close()

# Create main tkinter window
root = tk.Tk()
root.title("Weather App")

# Configure style for ttk widgets
style = ttk.Style()
style.configure('Custom.TButton', font=('Helvetica', 12))

# Create GUI elements
container_frame = ttk.Frame(root)
container_frame.pack(fill=tk.BOTH, expand=True)

# Load background image
background_image = Image.open("Background.jpg")
background_image = background_image.resize((root.winfo_screenwidth(), root.winfo_screenheight()), Image.ANTIALIAS)
background_photo = ImageTk.PhotoImage(background_image)
background_label = tk.Label(container_frame, image=background_photo)
background_label.place(x=0, y=0, relwidth=1, relheight=1)

header_label = ttk.Label(container_frame, text="Know Your Weather", font=("Helvetica", 16, "bold"))
header_label.grid(row=0, column=0, columnspan=2, pady=(300,10))

city_label = ttk.Label(container_frame, text="Enter your city name")
city_label.grid(row=1, column=0, padx=10, pady=5, sticky="e")

city_entry = ttk.Entry(container_frame, width=30, font=('Helvetica', 12))
city_entry.grid(row=1, column=1, padx=10, pady=5, sticky="w")

submit_button = ttk.Button(container_frame, text="Submit", command=fetch_weather, style='Custom.TButton')
submit_button.grid(row=2, column=0, columnspan=2, pady=10)

output_label = ttk.Label(container_frame, text="", font=("Helvetica", 12), justify="left", wraplength=600)
output_label.grid(row=3, column=0, columnspan=2, padx=10, pady=10)

# Run the tkinter event loop
root.mainloop()
